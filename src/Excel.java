import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;



public class Excel {
	
	/**
	 * 
	 * @param fileName 返回给前台的文件名称，根据扩展名导出不同格式03 or 07
	 * @param sheetName		sheet名字
	 * @param datas		导出到excel的数据
	 * @param header	title，如果采用数据字典这里可以在修改一下逻辑
	 * @return
	 */
	public static File WriteExcel(String fileName, String sheetName, JSONArray datas, String[][] header) {
		File file = new File(fileName);
		
		FileOutputStream fos = null;
		Workbook wb = null;
		int rowNum = 0;	// excel2003 一个sheet最大存储65536条数据
						// excel2007~ 1048576
		try {
			fos = new FileOutputStream(file);
			if (isExcel2003(file)){
				wb = new HSSFWorkbook();
				rowNum = 65535;
			}else{
				wb = new XSSFWorkbook();
				rowNum = 1048575;
			}
			
			int dataNum = datas.size();
			int sheetNum = (int) Math.ceil(((float)dataNum)/rowNum);
			
			if (sheetNum==0) {
				return null;
			} 
			
			
			if (sheetNum==1) {
				Sheet sheet = wb.createSheet(sheetName);
				Row row = sheet.createRow(0);
				Cell cell = null;
		        for (int i = 0; i < header.length; i++) {
		            cell = row.createCell((short) i);
		            cell.setCellType(CellType.STRING);
		            cell.setCellValue(header[i][1]);  
		        }
				
				for (int i=0; i<dataNum; i++) {
					row = sheet.createRow(i+1);	// 创建一行
					JSONObject data = datas.getJSONObject(i);	// 要导入的数据
					for (int j=0; j<header.length; j++) {
						cell = row.createCell(j);	// 默认全部按String类型导出
						cell.setCellValue(data.getString(header[j][0]));
					}
				}
				
			} else {
				int temNum = 0;
				int index = 0;
				for (int ii=1; ii<=sheetNum; ii++) {
					Sheet sheet = wb.createSheet(sheetName+"_"+ii);
					temNum = 0;
					Row row = sheet.createRow(0);
					Cell cell = null;
			        for (int i=0; i<header.length; i++) {
			            cell = row.createCell((short) i);
			            cell.setCellType(CellType.STRING);
			            cell.setCellValue(header[i][1]);  
			        }
					index = rowNum*(ii-1);
					for (int i=0; i<dataNum && (index+i)<dataNum; i++) {
						if (temNum==rowNum)break;
						System.out.println(i);
						row = sheet.createRow(i+1);	// 创建一行
						JSONObject data = datas.getJSONObject(index+i);	// 要导入的数据
						for (int j=0; j<header.length; j++) {
							cell = row.createCell(j);	// 默认全部按String类型导出
							cell.setCellValue(data.getString(header[j][0]));
						}
						temNum++;
					}
				}
				
			}
			
			wb.write(fos);
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				fos.close();
				wb.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return file;
	}
	
	public static void ReadExcel(File file) {
		if (!file.isFile() || !file.canRead()) {
			return;
		}
		FileInputStream fis = null;
		Workbook wb = null;
		try {
			fis = new FileInputStream(file);
			if (isExcel2003(file)){
				wb = new HSSFWorkbook(fis);
			}else{
				wb = new XSSFWorkbook(fis);
			}
			
			for (Sheet as : wb) {
				for (Row ar : as) {
					for (Cell ac : ar) {
						System.out.print(ac.getStringCellValue()+" ");	// 真实数据，这里得按照你的需要改一下
					}
					System.out.println();	// 换行输出
				}
				System.out.println("end sheet...");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				fis.close();
				wb.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}
	
	public static boolean isExcel2003 (File file) {
		if (file.getName().toLowerCase().endsWith("xls"))
			return true;
		return false;
	}
	
	public static void main(String[] args) {
		File file = new File("C:/Users/L/Desktop/test.xls");
		//Excel.ReadExcel(file);
		
		JSONArray ja = new JSONArray();
		for (int i=0; i<1048575; i++) {
			JSONObject jo = new JSONObject();
			jo.put("name", "嗯哼"+(i+1));
			jo.put("age", i);
			ja.add(jo);
		}
		
		// 最终导出后将name列显示成“姓名”。。。
		String[][] header = {
				{"name", "姓名"},
				{"age", "年龄"}
		};
		Excel.WriteExcel("C:/Users/L/Desktop/test1.xlsx", "这是一个测试", ja, header);
	}
}
